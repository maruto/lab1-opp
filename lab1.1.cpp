#include "mpi.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

const float epsilon = 0.00001f;

void printVect(float* x, int size, int ProcRank) {
    if(ProcRank == 0) {
        std::cout << '\n';
        for(int i = 0; i < size ; ++ i)
            std::cout << x[i] << '\n';
    }
}

void copyFirstVectorToSecond(const float *vector1, float *vector2, int vectorSize){
    for (int i = 0; i < vectorSize; i++) {
        vector2[i] = vector1[i];
    }
}

void mul(const float* partA, const float* x,  float* res, int size, const int* shift, const int* countRowsAtProc, int ProcRank) {
    for (int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
        res[shift[ProcRank] + i] = 0.0f;
        for (int j = 0; j < size; ++j) {
            res[shift[ProcRank] + i] += partA[i * size + j] * x[j];
        }
    }
    float *buffer = new float [size];

    MPI_Allgatherv((res+shift[ProcRank]), countRowsAtProc[ProcRank], MPI_FLOAT, buffer, countRowsAtProc, shift, MPI_FLOAT, MPI_COMM_WORLD);
    copyFirstVectorToSecond(buffer, res, size);

    delete[] buffer;
}

float scalarMul(const float* x, const float* y, int size) {
    float mul = 0.0f;
    for(int i = 0; i < size; ++i)
        mul += x[i]*y[i];
    return mul;
}
float vectorAbs(const float* x, int size) {
    float len = 0.0f;
    for(int i = 0; i < size; ++i)
        len += x[i]*x[i];
    return len;
}
void vectorSub(const float* x, const float* y, float* res, float k, int size){
    for(int i = 0; i < size; ++i)
        res[i] = x[i] - k*y[i];
}


float* nextX(float* x, float* partA, const float* b,  float* Ax,  float* Ay, int size, int* shift, int* countRowsAtProc, int ProcRank) {
    float *y = new float [size];
    vectorSub(Ax, b, y, 1, size);

    mul(partA, y, Ay, size, shift, countRowsAtProc, ProcRank);

    float t;
    if(ProcRank == 0)
        t = scalarMul(y, Ay, size) / scalarMul(Ay, Ay, size);
    MPI_Bcast(&t, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);


    float* part_x_plus_one = new float[size];
    vectorSub(x, y, part_x_plus_one, t, size);
    copyFirstVectorToSecond(part_x_plus_one, x, size);
    delete [] part_x_plus_one;

    delete [] y;
    return x;
}

bool ending(float* x, float* partA, float* b, float* Ax, int size, int* shift, int* countRowsAtProc, int ProcRank){
    mul(partA, x, Ax, size, shift, countRowsAtProc, ProcRank);

    float* sub = new float [size];
    vectorSub(Ax, b, sub, 1, size);

    float div;
    if(ProcRank == 0)
        div = (vectorAbs(sub, size) / vectorAbs(b, size));

    MPI_Bcast(&div, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    bool res = (div < epsilon * epsilon);

    delete [] sub;

    return res;
}

void init_1(float* &x, float* &partA, float* &b, float* &Ax, float* &Ay, int size, int ProcRank, int ProcNum, int* &shift, int* &countRowsAtProc) {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    shift = new int [ProcNum]; //смещение по элементам от начала для каждого процесса
    countRowsAtProc = new int [ProcNum]; // колво элементов которое процесс считает

    int remainder = size % ProcNum; //остаток
    int quotient = size / ProcNum; //частное

    int startLine;//номер строки с которой процесс считает матрицу A
    int NumberOfLines;//количествно строк которые обрабатывает каждый процесс
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(ProcRank < remainder) {
        NumberOfLines = quotient + 1;
        startLine = (ProcRank) * (NumberOfLines);

        partA = new float [NumberOfLines*size];

        for(int i = 0; i < NumberOfLines; i++) {
            for(int j = 0; j < size; j++) {
                partA[i * size + j]= 1.0f;
            }
            partA[i * size + startLine + i] = 2.0f;
        }
    } else {
        NumberOfLines = quotient;
        startLine = (remainder * (NumberOfLines + 1)) + ((ProcRank - remainder) * NumberOfLines);

        partA = new float [NumberOfLines*size];

        for (int i = 0; i < NumberOfLines; ++i) {
            for(int j = 0; j < size; ++j) {
                partA[i * size + j] = 1.0f;
            }
            partA[i * size + startLine + i] = 2.0f;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&NumberOfLines, 1, MPI_INTEGER, i, 0, (countRowsAtProc+i), 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Sendrecv(&startLine, 1, MPI_INTEGER, i, 0, (shift+i), 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    float* u = new float [size];

    b = new float [size];
    for(int i = 0; i < size; ++i) {
        u[i] = (float)(rand() % 600) - 300;
    }

    if(ProcRank == 0) {
        std::cout << "\nVector X should look like this:\n";
        printVect(u, size, ProcRank);
        std::cout << "End of vector.\n";
    }
    mul(partA, u, b, size, shift, countRowsAtProc, ProcRank);

    x = new float [size];
    for(int i = 0; i < size; ++i) {
        x[i] = 0.0f;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Ax = new float [size];
    Ay = new float [size];
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    delete[] u;
}

int main(int argc, char *argv[]) {
    struct timespec start, finish;

    MPI_Init( &argc, &argv );

    int size =  1000; //Размер матрицы и вектора
    float* partA; //часть матрицы коэффицентов для каждого процесса она своя
    float* b; //вектор правых частей
    float* x; //вектор значений

    float* Ax; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор x
    float* Ay; //вспомогательный вектор, хранит в себе результат умножения матрицы A на вектор y

    int ProcNum; //количество выполняемых процессов
    int ProcRank; //номер текущего процесса(нумерация с нуля)

    int *countRowsAtProc; // массив, который хранит в i-ой позиции количество строк которые считает i-ый процесс
    int *shift; //массив, кото хранит в хранит в i-ой позиции смещение(в строках) блока с данными относительно начала матрицы для i-ого процесса

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    MPI_Comm_size ( MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank ( MPI_COMM_WORLD, &ProcRank);

    init_1(x, partA, b, Ax, Ay, size, ProcRank, ProcNum, shift, countRowsAtProc);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if (ProcRank == 0) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    }

    while(!ending(x, partA, b, Ax, size, shift, countRowsAtProc, ProcRank)) {
        x = nextX(x, partA, b,  Ax, Ay, size, shift, countRowsAtProc, ProcRank);
    }

    if (ProcRank == 0) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &finish);
        std::cout << "Time: " << (finish.tv_sec - start.tv_sec + 0.000000001 * (finish.tv_nsec - start.tv_nsec)) << '\n';

        std::cout << "\nResult of the program:\n";
    }
    printVect(x, size, ProcRank);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    delete [] partA;
    delete [] b;
    delete [] x;
    delete [] Ax;
    delete [] Ay;

    delete [] shift;
    delete [] countRowsAtProc;

    MPI_Finalize();
    return 0;
}
