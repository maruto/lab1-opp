#include "mpi.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

const float epsilon = 0.00001f;

void printVect(float* partVect, int* countRowsAtProc, int ProcRank, int ProcNum) {
    for(int p = 0; p < ProcNum; ++p) {
        if(ProcRank == p){
            for(int i = 0; i < countRowsAtProc[ProcRank]; ++i)
                std::cout << partVect[i] << '\n';
        }
        MPI_Barrier(MPI_COMM_WORLD);
    }
}

void copyFirstVectorToSecond(const float *vector1, float *vector2, int vectorSize){
    for (int i = 0; i < vectorSize; ++i) {
        vector2[i] = vector1[i];
    }
}

void mul(const float* partA, const float* partX,  float* partRes, int size, const int* shift, const int* countRowsAtProc, int ProcRank, int ProcNum) {
    float* buffer = new float [countRowsAtProc[0]];
    copyFirstVectorToSecond(partX, buffer, countRowsAtProc[ProcRank]);

    for(int i = 0; i < countRowsAtProc[ProcRank]; ++i)
        partRes[i] = 0.0f;

    for(int p = ProcRank, counter = 0; counter < ProcNum; counter++, p = (p+1)%ProcNum){

        for (int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
            for (int j = 0; j < countRowsAtProc[p]; ++j) {
                partRes[i] += partA[i * size + (j + shift[p])] * buffer[j];
            }
        }
        MPI_Sendrecv_replace(buffer, countRowsAtProc[0], MPI_FLOAT, (ProcRank-1+ProcNum)%ProcNum, 5, (ProcRank+1)%ProcNum, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    delete [] buffer;
}

float scalarMul(const float* partX, const float* partY, const int* countRowsAtProc, int ProcRank, int ProcNum) {
    float partMul = 0.0f;
    for(int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
        partMul += partX[i]*partY[i];
    }
    float resMul = 0.0f;

    MPI_Allreduce( &partMul, &resMul, 1, MPI_FLOAT, MPI_SUM,  MPI_COMM_WORLD);
    return resMul;
}

float vectorAbs(const float* partX, const int* shift, const int* countRowsAtProc, int ProcRank) {
    float partLen = 0.0f;
    for(int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
        partLen += partX[i] * partX[i];
    }
    float resLen = 0.0f;

    MPI_Allreduce( &partLen, &resLen, 1, MPI_FLOAT, MPI_SUM,  MPI_COMM_WORLD);
    return resLen;
}

void vectorSub(const float* x, const float* y, float* res, float k, const int* countRowsAtProc, int ProcRank){
    for(int i = 0; i < countRowsAtProc[ProcRank]; ++i) {
        res[i] = x[i] - k * y[i];
    }
}

//Procedure for finding the next vector X
float* nextX(float* partX, float* partA, const float* partB,  float* partAx,  float* partAy, int size, int* shift, int* countRowsAtProc, int ProcRank, int ProcNum) {
    float *partY = new float [countRowsAtProc[ProcRank]];

    vectorSub(partAx, partB, partY, 1, countRowsAtProc, ProcRank);

    mul(partA, partY, partAy, size, shift, countRowsAtProc, ProcRank, ProcNum);

    float smYAy = scalarMul(partY,  partAy, countRowsAtProc,ProcRank, ProcNum);
    float smAyAy = scalarMul(partAy,  partAy, countRowsAtProc,ProcRank, ProcNum);
    float t;
    if(ProcRank == 0)
        t = smYAy / smAyAy;
    MPI_Bcast(&t, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);

    float* part_x_plus_one = new float[countRowsAtProc[ProcRank]];
    vectorSub(partX, partY, part_x_plus_one, t, countRowsAtProc, ProcRank);
    copyFirstVectorToSecond(part_x_plus_one, partX, countRowsAtProc[ProcRank]);

    delete [] part_x_plus_one;

    delete [] partY;

    return partX;
}

//Function for checking the achievement of the result completion condition
bool ending(float* partX, float* partA, float* partB, float* partAx, int size, int* shift, int* countRowsAtProc, int ProcRank, int ProcNum){
    mul(partA, partX, partAx, size, shift, countRowsAtProc, ProcRank, ProcNum);

    float* partSub = new float [countRowsAtProc[ProcRank]];

    vectorSub(partAx, partB, partSub, 1, countRowsAtProc, ProcRank);

    float subAbs = vectorAbs(partSub, shift, countRowsAtProc, ProcRank);
    float bAbs = vectorAbs(partB, shift, countRowsAtProc, ProcRank);
    float div;
    if(ProcRank == 0)
        div = subAbs / bAbs;
    MPI_Bcast(&div, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    bool res = (div < epsilon * epsilon);

    delete [] partSub;

    return res;
}

//Function for initializing matrices
void init_2(float* &partX, float* &partA, float* &partB, float* &partAx, float* &partAy, int size, int ProcRank, int ProcNum, int* &shift, int* &countRowsAtProc) {
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    shift = new int [ProcNum];
    countRowsAtProc = new int [ProcNum];

    int remainder = size % ProcNum; //остаток
    int quotient = size / ProcNum; //частное

    int startLine;//номер строки с которой процесс считает матрицу A
    int NumberOfLines;//количествно строк которые обрабатывает каждый процесс
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(ProcRank < remainder) {
        NumberOfLines = quotient + 1;
        startLine = (ProcRank) * (NumberOfLines);

        partA = new float [NumberOfLines*size];

        for(int i = 0; i < NumberOfLines; ++i) {
            for(int j = 0; j < size; ++j) {
                partA[i * size + j]= 1.0f;
            }
            partA[i * size + startLine + i] = 2.0f;
        }
    } else {
        NumberOfLines = quotient;
        startLine = (remainder * (NumberOfLines + 1)) + ((ProcRank - remainder) * NumberOfLines);

        partA = new float [NumberOfLines*size];

        for (int i = 0; i < NumberOfLines; ++i) {
            for(int j = 0; j < size; ++j) {
                partA[i * size + j] = 1.0f;
            }
            partA[i * size + startLine + i] = 2.0f;
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    for (int i = 0; i < ProcNum; ++i) {
        MPI_Sendrecv(&NumberOfLines, 1, MPI_INTEGER, i, 0, (countRowsAtProc+i), 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        MPI_Sendrecv(&startLine, 1, MPI_INTEGER, i, 0, (shift+i), 1, MPI_INTEGER, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    float* partU = new float [NumberOfLines];
    for(int i = 0; i < NumberOfLines; ++i) {
        partU[i] = (rand() % 600) - 300;
    }


    if(ProcRank == 0)
        std::cout << "\nVector X should look like this:\n";
    printVect(partU, countRowsAtProc, ProcRank, ProcNum);
    if(ProcRank == 0)
        std::cout << "End of vector.\n";

    partB = new float [NumberOfLines];
    mul(partA, partU, partB, size, shift, countRowsAtProc, ProcRank, ProcNum);
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    partX = new float [NumberOfLines];
    for(int i = 0; i < NumberOfLines; ++i) {
        partX[i] = 0.0f;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    partAx = new float [NumberOfLines];
    partAy = new float [NumberOfLines];
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////
    delete [] partU;
}

int main(int argc, char *argv[]) {

    struct timespec start, finish;

    MPI_Init( &argc, &argv );

    int size =  100; //Размер матрицы и вектора
    float* partA; //часть матрицы коэффицентов для каждого процесса она своя
    float* partB; //часть вектор правых частей -||-
    float* partX; //часть вектор значений -||-


    float* partAx; // часть вспомогательного вектора, хранит в себе результат умножения матрицы A на вектор x
    float* partAy; //часть вспомогательного вектора, хранит в себе результат умнодения матрицы A на вектор y

    int ProcNum; //количество выполняемых процессов
    int ProcRank; //номер текущего процесса(нумерация с нуля)

    int *countRowsAtProc; // массив, который хранит в i-ой позиции колличество строк которые считает i-ый процесс
    int *shift; //массив, кото хранит в хранит в i-ой позиции смещение(в строках) блока с данными относительно начала матрицы для i-ого процесса

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    MPI_Comm_size ( MPI_COMM_WORLD, &ProcNum);
    MPI_Comm_rank ( MPI_COMM_WORLD, &ProcRank);

    init_2(partX, partA, partB, partAx, partAy, size, ProcRank, ProcNum, shift, countRowsAtProc);


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if (ProcRank == 0) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &start);
    }

    while(!ending(partX, partA, partB, partAx, size, shift, countRowsAtProc, ProcRank, ProcNum)) {
        partX = nextX(partX, partA, partB,  partAx, partAy, size, shift, countRowsAtProc, ProcRank, ProcNum);
    }

    if (ProcRank == 0) {
        clock_gettime(CLOCK_MONOTONIC_RAW, &finish);
        std::cout << "Time: " << (finish.tv_sec - start.tv_sec + 0.000000001 * (finish.tv_nsec - start.tv_nsec)) << '\n';

        std::cout << "\nResult of the program:\n";
    }
    printVect(partX, countRowsAtProc, ProcRank, ProcNum);
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    delete [] partA;
    delete [] partB;
    delete [] partX;
    delete [] partAx;
    delete [] partAy;

    delete [] shift;
    delete [] countRowsAtProc;

    MPI_Finalize();
    return 0;
}